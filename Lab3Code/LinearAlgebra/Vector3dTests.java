//Robert Gutkowski 1810515
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests 
{
    
    @Test
    public void testGetX() 
    {
        Vector3d one = new Vector3d(1,2,3);
        assertEquals(1, one.getX());
    }
    @Test
    public void testGetY() 
    {
        Vector3d two = new Vector3d(4,5,6);
        assertEquals(5, two.getY());
    }
    @Test
    public void testGetZ() 
    {
        Vector3d three = new Vector3d(7,6,53);
        assertEquals(53, three.getZ());
    }
    @Test
    public void testMagnitude() 
    {
        Vector3d four = new Vector3d(1,2,-2);
        assertEquals(3, four.magnitude());
    }
    @Test
    public void testDotProduct() 
    {
        Vector3d five = new Vector3d(1,2,3);
        Vector3d six = new Vector3d(5,1,2);
        assertEquals(13, five.dotProduct(six));
    }
    @Test
    public void testAdd() 
    {
        Vector3d seven = new Vector3d(1,2,3);
        Vector3d eight = new Vector3d(4,5,6);
        
        Vector3d newVector = seven.add(eight);
        Vector3d result = new Vector3d(5,7,9);
        assertEquals(result.getX()+" "+result.getY()+" "+result.getZ(), newVector.getX()+" "+newVector.getY()+" "+newVector.getZ());
    }

    
}
