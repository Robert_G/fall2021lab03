//Robert Gutkowski 1810515
package LinearAlgebra;
public class Vector3d
{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public double getX()
    {
        return this.x;
    }

    public double getY()
    {
        return this.y;
    }

    public double getZ()
    {
        return this.z;
    }

    
    public double magnitude()
    {
        return Math.sqrt( Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2) );
    }

    public double dotProduct(Vector3d dot)
    {
        return (this.x * dot.getX()) + (this.y * dot.getY()) + (this.z * dot.getZ());
    }

    public Vector3d add(Vector3d secondVector)
    {
        double totalX = this.x + secondVector.getX();
        double totalY = this.y + secondVector.getY();
        double totalZ = this.z + secondVector.getZ();
        Vector3d newVector = new Vector3d(totalX, totalY, totalZ);
        return newVector;
    }
}