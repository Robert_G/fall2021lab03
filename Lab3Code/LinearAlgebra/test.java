package LinearAlgebra;

public class test 
{
    public static void main(String[] args)
    {
        Vector3d one = new Vector3d(1,2,3);
        Vector3d two = new Vector3d(4,5,6);
        System.out.println("x: "+one.getX()+" y: "+one.getY()+" z: "+one.getZ());
        System.out.println("x: "+two.getX()+" y: "+two.getY()+" z: "+two.getZ());
        System.out.println(one.magnitude()+"\n"+two.magnitude());
        System.out.println(one.dotProduct(two));
        Vector3d total = one.add(two);
        System.out.println("x: "+total.getX()+" y: "+total.getY()+" z: "+total.getZ());

    }
}
